using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

public class Item : MonoBehaviour, IPointerDownHandler,IPointerEnterHandler,IPointerExitHandler, IEndDragHandler,
                                   IBeginDragHandler, IDragHandler
{

    [SerializeField] private AudioSource _source;

    
    private static Transform _dragFrom;
    public static Item _draggingItem;

    [SerializeField] private Image _myImage;
    private bool _yes = true;

    // Начало перетаскивания.
    public void OnBeginDrag(PointerEventData eventData)
    {
        _draggingItem = this;

        _dragFrom = _draggingItem.transform;

        _myImage.raycastTarget = false;
        _yes = true;
    }



    private void Update()
    {
        if (_yes == false && _draggingItem != null)
        {
            Drop(_dragFrom);
        }
    }



    private void Drop(Transform _objTransform)
    {
        _draggingItem.transform.SetParent(_objTransform);
        _draggingItem.transform.localPosition = Vector3.zero;
        
        _dragFrom = null;
       _draggingItem = null;
    }

    // Передвигает компонент image.
    public void OnDrag(PointerEventData eventData)
    {
        _dragFrom.position = eventData.position;
    }

    
    public void OnEndDrag(PointerEventData _objTransform)
    {
        _yes = false;
        _myImage.raycastTarget = true;
    }
    

    public void OnPointerDown(PointerEventData eventData)
    {
        _source.Play();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        transform.localScale = new Vector2(1.5f, 1.5f);
    }

    void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
    {
        transform.localScale = new Vector2(1f, 1f);
    }
}
