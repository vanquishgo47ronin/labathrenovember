using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSource;

    [SerializeField] private AudioClip _audioClip;
    
    public void PlaySound()
    {
        if (!_audioSource.isPlaying)
        {
            _audioSource.clip = _audioClip;
            _audioSource.Play();
        }
    }
}
