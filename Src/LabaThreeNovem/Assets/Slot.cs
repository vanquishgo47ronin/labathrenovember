using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class Slot : MonoBehaviour, IDropHandler
{
    private Item _item;

    public void OnDrop(PointerEventData eventData)
    {
        _item = Item._draggingItem;
        _item.transform.SetParent(transform);
        
    }
}
